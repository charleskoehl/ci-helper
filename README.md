## ci-helper
##### Command-line utility for use with Kinvey and Bitbucket.

### Usage
`npm i -g ci-helper`

##### Generate and output OTP to console:
`ci-helper --op=otp --secret=xyz123`

##### Get aspect of Kinvey status such as 'version', 'status'. 
`ci-helper --op=get-kinvey-status --key=version`

##### Wait for Kinvey Deployment:
```
In order for this to work you have to:
1. Make sure these environment variables are set:
 - PACKAGE_VERSION
 - BITBUCKET_DEPLOYMENT_ENVIRONMENT (preset by Bitbucket)
 - KINVEY_USER_SECRET
 - KINVEY_SERVICE_ID
2. kinvey profile create <profile name>
3. kinvey flex deploy

Then call:

ci-helper --op=wait-kinvey-deploy
```
