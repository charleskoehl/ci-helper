# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.8.28](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.27...v1.8.28) (2020-02-21)


### Bug Fixes

* default for non-stored var ([5966cfb](https://bitbucket.org/charleskoehl/ci-helper/commit/5966cfb85e2c1e545ce48e1f17ef4603350f44e9))
* params defaults for getKinveyStatus ([6b949ca](https://bitbucket.org/charleskoehl/ci-helper/commit/6b949ca04e1ab6784ca6f672a34a4085740ba5ae))

### [1.8.27](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.26...v1.8.27) (2020-02-21)


### Bug Fixes

* error handling ([2437c31](https://bitbucket.org/charleskoehl/ci-helper/commit/2437c318ac43e20696de0613ee0e8ecdf670a12b))

### [1.8.26](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.25...v1.8.26) (2020-02-21)


### Bug Fixes

* error handling ([15b54c3](https://bitbucket.org/charleskoehl/ci-helper/commit/15b54c373971ed987b4dac3310351233b0a9da74))

### [1.8.25](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.24...v1.8.25) (2020-02-21)


### Bug Fixes

* createKinveyProfile creds build ([0231117](https://bitbucket.org/charleskoehl/ci-helper/commit/0231117c37ade73fb384199340b68e043eace9a2))

### [1.8.24](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.23...v1.8.24) (2020-02-21)

### [1.8.23](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.22...v1.8.23) (2020-02-21)


### Bug Fixes

* createKinveyProfile creds build ([e4258ad](https://bitbucket.org/charleskoehl/ci-helper/commit/e4258ad8b1de69a20c90a7d10e643e5877170786))

### [1.8.22](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.21...v1.8.22) (2020-02-21)


### Bug Fixes

* createKinveyProfile creds build ([3ee5aa1](https://bitbucket.org/charleskoehl/ci-helper/commit/3ee5aa1efb985124429ff4f13bc84a8b15515223))

### [1.8.21](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.20...v1.8.21) (2020-02-21)


### Bug Fixes

* otplib import ([6c7bb3a](https://bitbucket.org/charleskoehl/ci-helper/commit/6c7bb3a0159b423ba46eeceb7bec3df28e2eb6c3))

### [1.8.20](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.19...v1.8.20) (2020-02-21)

### [1.8.19](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.18...v1.8.19) (2020-02-21)

### [1.8.18](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.17...v1.8.18) (2020-02-21)


### Bug Fixes

* createKinveyProfile creds build ([712fd58](https://bitbucket.org/charleskoehl/ci-helper/commit/712fd585a18f732a4300ca6b49bab832820dfe6c))

### [1.8.17](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.16...v1.8.17) (2020-02-21)


### Bug Fixes

* createKinveyProfile creds build ([1ae2644](https://bitbucket.org/charleskoehl/ci-helper/commit/1ae2644cdaeb7e7dd4cbbb389cd87255430244f9))

### [1.8.16](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.15...v1.8.16) (2020-02-21)


### Bug Fixes

* createKinveyProfile creds build ([e090a8f](https://bitbucket.org/charleskoehl/ci-helper/commit/e090a8fc31b3bcdf66a1b894efd54e36d26badcb))

### [1.8.15](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.14...v1.8.15) (2020-02-21)


### Bug Fixes

* storing default neo4j vars ([7b95ac6](https://bitbucket.org/charleskoehl/ci-helper/commit/7b95ac639c0d255ca4913710d03c83fb0def0804))

### [1.8.14](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.13...v1.8.14) (2020-02-21)


### Bug Fixes

* url validation ([51334eb](https://bitbucket.org/charleskoehl/ci-helper/commit/51334ebf8dd0ba4e1d0af1dc94871ec9100cd207))

### [1.8.13](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.12...v1.8.13) (2020-02-21)


### Bug Fixes

* split serviceIdsStr ([69c4133](https://bitbucket.org/charleskoehl/ci-helper/commit/69c4133889eee60ca8642a338acefa43f4c2dc13))

### [1.8.12](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.11...v1.8.12) (2020-02-21)

### [1.8.11](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.10...v1.8.11) (2020-02-21)


### Bug Fixes

* in waitForKinveyDeployment func, always show test value before checking if equal to target and exiting ([0dcced5](https://bitbucket.org/charleskoehl/ci-helper/commit/0dcced5adcac66fee3bd8cb75ac10a218a509b58))

### [1.8.10](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.9...v1.8.10) (2020-02-21)


### Bug Fixes

* in waitForKinveyDeployment func, always show test value before checking if equal to target and exiting ([f16bdf2](https://bitbucket.org/charleskoehl/ci-helper/commit/f16bdf2e8193823eeb1cd4f5be871426cd13af56))

### [1.8.9](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.8...v1.8.9) (2020-02-21)


### Bug Fixes

* in waitForKinveyDeployment func, always show test value before checking if equal to target and exiting ([44c2952](https://bitbucket.org/charleskoehl/ci-helper/commit/44c29524c717f085e258e36bcbcd1af5a66d87f7))

### [1.8.8](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.7...v1.8.8) (2020-02-21)


### Bug Fixes

* silence getKinveyStatus ([a96cab4](https://bitbucket.org/charleskoehl/ci-helper/commit/a96cab48de0425225dba3354adddf74c57073db1))

### [1.8.7](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.6...v1.8.7) (2020-02-21)


### Bug Fixes

* UNDO: get token only once for waitForKinveyDeployment (token was expiring) ([d25dcf8](https://bitbucket.org/charleskoehl/ci-helper/commit/d25dcf877b230e35c5df2ac3731c749ca6d489a3))

### [1.8.6](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.5...v1.8.6) (2020-02-21)

### [1.8.5](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.4...v1.8.5) (2020-02-21)


### Bug Fixes

* getOtp params default ([7c5b537](https://bitbucket.org/charleskoehl/ci-helper/commit/7c5b5375450086baa45fab45ecc22f8340bc0676))

### [1.8.4](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.3...v1.8.4) (2020-02-21)


### Bug Fixes

* fall back to env var for kinveyUserSecret in createKinveyProfile func ([a0ae49a](https://bitbucket.org/charleskoehl/ci-helper/commit/a0ae49ace4eddb5af94f50b472eb39a0995eb574))

### [1.8.3](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.2...v1.8.3) (2020-02-21)


### Bug Fixes

* default secret to env var for getOtp ([691d6cd](https://bitbucket.org/charleskoehl/ci-helper/commit/691d6cdb7897ecf393cad528e97515e25765ecf7))
* get token only once for waitForKinveyDeployment ([4db3929](https://bitbucket.org/charleskoehl/ci-helper/commit/4db39296426f88d8d33216a634103390e78f629e))
* pass correct params to getKinveyStatus ([6c86b37](https://bitbucket.org/charleskoehl/ci-helper/commit/6c86b37cc8fed96e4c4307fe7ddf4db12e44125d))

### [1.8.2](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.1...v1.8.2) (2020-02-21)

### [1.8.1](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.8.0...v1.8.1) (2020-02-21)


### Bug Fixes

* error handling ([0556c80](https://bitbucket.org/charleskoehl/ci-helper/commit/0556c80b8ec54c0e36a903ec70b24942345bb514))

## [1.8.0](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.7.2...v1.8.0) (2020-02-21)


### Features

* op 'set-neo4j-vars' ([2fbf9cc](https://bitbucket.org/charleskoehl/ci-helper/commit/2fbf9cc215c40d079b343abeb5bd87efd3f25133))


### Bug Fixes

* use sustainHawaiiCiHelper command instead of ci-helper ([642b6fd](https://bitbucket.org/charleskoehl/ci-helper/commit/642b6fd78ded7d68c9319c1a3d0c61155bb5a282))

### [1.7.2](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.7.1...v1.7.2) (2020-02-21)


### Bug Fixes

* UNDO use sustainHawaiiCiHelper command instead of ci-helper ([da6326d](https://bitbucket.org/charleskoehl/ci-helper/commit/da6326db0e873d5ae596252a604d7111c0026199))

### [1.7.1](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.7.0...v1.7.1) (2020-02-21)


### Bug Fixes

* put "main" back into package.json because the "bin" config doesn't work in bitbucket-pipeline ([d25e2fd](https://bitbucket.org/charleskoehl/ci-helper/commit/d25e2fd52abdccac4ad10f2bccf16af18bb54f2a))

## [1.7.0](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.10...v1.7.0) (2020-02-21)


### Features

* create-kinvey-profile op ([a939f00](https://bitbucket.org/charleskoehl/ci-helper/commit/a939f00585e7804c44c7c6db6e458b24eb570f93))
* createKinveyProfile function ([e1d6e82](https://bitbucket.org/charleskoehl/ci-helper/commit/e1d6e823f665a1b72f6a89790f5c49ee03625532))
* createKinveyProfile function ([7a79e62](https://bitbucket.org/charleskoehl/ci-helper/commit/7a79e62a167f352f62a8c27e32e3ce3865c5c819))
* make executable using sustainHawaiiCiHelper command ([dd167dc](https://bitbucket.org/charleskoehl/ci-helper/commit/dd167dc0b75ee245ffbaec2128b6dd879618bb26))
* setKinveyServiceVars ([299d168](https://bitbucket.org/charleskoehl/ci-helper/commit/299d168ef0b328abaaed062368ce24481a35b33b))
* setNeo4jVars ([ba55192](https://bitbucket.org/charleskoehl/ci-helper/commit/ba55192fc94d086af4b79b716b0db142b45bf4d4))
* setNeo4jVars ([680b149](https://bitbucket.org/charleskoehl/ci-helper/commit/680b14927a0c93a3ed92fef9a73c97ee96fc428d))
* setNeo4jVars ([6a6e3b4](https://bitbucket.org/charleskoehl/ci-helper/commit/6a6e3b45c2d29c669ce4a950bc72d2f42b5d8250))


### Bug Fixes

* add optional token param to getKinveyStatus ([7d79b19](https://bitbucket.org/charleskoehl/ci-helper/commit/7d79b19698abc20665e5a926a7ac75e4281b5c01))
* allow overrides of getKinveyStatus params ([b9ffc2d](https://bitbucket.org/charleskoehl/ci-helper/commit/b9ffc2d0215593d6cf8ae2c68e46af4b254fe937))
* skip waiting for correct packagechore: refactor ([72a28c2](https://bitbucket.org/charleskoehl/ci-helper/commit/72a28c2ed3b63ecddaeda1adf14ee6d579c115d7))

### [1.6.3](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.2...v1.6.3) (2019-12-20)

### [1.6.10](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.9...v1.6.10) (2020-02-11)


### Bug Fixes

* exit with error if any stages of kinvey deployment fail ([af5b1ac](https://bitbucket.org/charleskoehl/ci-helper/commit/af5b1ac1aca76c488d78b1d4f08af702b1206c31))

### [1.6.9](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.8...v1.6.9) (2019-12-20)


### Bug Fixes

* params for getOtp call ([d8a275b](https://bitbucket.org/charleskoehl/ci-helper/commit/d8a275ba526b7212e276bb0e6da701145cf2f8cc))

### [1.6.8](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.7...v1.6.8) (2019-12-20)


### Bug Fixes

* output otp async ([33ef52c](https://bitbucket.org/charleskoehl/ci-helper/commit/33ef52cac145a3dd46186b24ab5ce7c6aff1990a))

### [1.6.7](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.6...v1.6.7) (2019-12-20)

### [1.6.6](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.5...v1.6.6) (2019-12-20)


### Bug Fixes

* get token async in getKinveyStatus ([19c7774](https://bitbucket.org/charleskoehl/ci-helper/commit/19c7774503603d7573c692ddf475e569f2336ca6))

### [1.6.5](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.2...v1.6.5) (2019-12-20)

### [1.6.3](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.2...v1.6.3) (2019-12-20)

### [1.6.2](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.1...v1.6.2) (2019-12-20)

### [1.6.1](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.6.0...v1.6.1) (2019-12-20)


### Bug Fixes

* ensure a minimum seconds remaining for 2fa code ([c6ecc84](https://bitbucket.org/charleskoehl/ci-helper/commit/c6ecc841dc69c0ac732e2b6700e3740579efd83e))

## [1.6.0](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.5.1...v1.6.0) (2019-12-20)


### Features

* ensure a minimum seconds remaining for 2fa code ([0b2bd0a](https://bitbucket.org/charleskoehl/ci-helper/commit/0b2bd0ae26e08f0d9e1447f8a80130f05d7d2d87))

### [1.5.1](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.5.0...v1.5.1) (2019-12-20)

## [1.5.0](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.25...v1.5.0) (2019-12-20)


### Features

* ensure a minimum seconds remaining for 2fa code ([558c85e](https://bitbucket.org/charleskoehl/ci-helper/commit/558c85e0f639b387129770eed728cade679d0c80))

### [1.4.25](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.24...v1.4.25) (2019-11-06)


### Bug Fixes

* eliminate unnecessary delay ([c0525c7](https://bitbucket.org/charleskoehl/ci-helper/commit/c0525c7c494acc7fcc58e4ee624c36be794f41a1))
* silence exec output ([9a7dac3](https://bitbucket.org/charleskoehl/ci-helper/commit/9a7dac3c4f08fbc84615c5056185d29dce2a3794))

### [1.4.24](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.23...v1.4.24) (2019-11-06)


### Bug Fixes

* get status before first test ([e7b98e8](https://bitbucket.org/charleskoehl/ci-helper/commit/e7b98e85c39fe789b0ddeb6d11a094f6591249ed))

### [1.4.23](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.22...v1.4.23) (2019-11-06)

### [1.4.22](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.21...v1.4.22) (2019-11-06)


### Bug Fixes

* get status before first test ([815f160](https://bitbucket.org/charleskoehl/ci-helper/commit/815f160d32d65e531b7f66db733cd65b6fb88514))

### [1.4.21](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.20...v1.4.21) (2019-11-06)


### Bug Fixes

* trim result of kinvey status ([0ea59a2](https://bitbucket.org/charleskoehl/ci-helper/commit/0ea59a2923343d75fbe5ba008598e7007da6477c))

### [1.4.20](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.19...v1.4.20) (2019-11-05)

### [1.4.19](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.18...v1.4.19) (2019-11-05)


### Bug Fixes

* trim result of kinvey status ([7582db1](https://bitbucket.org/charleskoehl/ci-helper/commit/7582db1fb00562806a8096151d25f173af2190af))

### [1.4.18](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.17...v1.4.18) (2019-11-05)

### [1.4.17](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.16...v1.4.17) (2019-11-05)

### [1.4.16](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.15...v1.4.16) (2019-11-05)

### [1.4.15](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.14...v1.4.15) (2019-11-05)


### Bug Fixes

* use async.whilst and async.everySeries ([50b8a54](https://bitbucket.org/charleskoehl/ci-helper/commit/50b8a54002d30fb392802fa5fe3bc905775f6c65))

### [1.4.14](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.13...v1.4.14) (2019-11-05)


### Bug Fixes

* use async.whilst and async.everySeries ([bb1667c](https://bitbucket.org/charleskoehl/ci-helper/commit/bb1667c895893e70e6c9b1c5f0100d2483bbd960))

### [1.4.13](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.12...v1.4.13) (2019-11-05)


### Bug Fixes

* use async.whilst ([4f14a2e](https://bitbucket.org/charleskoehl/ci-helper/commit/4f14a2e317219e27f59e9121dd2de48c15536a86))

### [1.4.12](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.11...v1.4.12) (2019-11-05)


### Bug Fixes

* use sleep npm module instead of exec ([141abd8](https://bitbucket.org/charleskoehl/ci-helper/commit/141abd8bfc676fa0be9b5eabda15643e6bc255d3))

### [1.4.11](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.10...v1.4.11) (2019-11-05)

### [1.4.10](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.9...v1.4.10) (2019-11-05)


### Bug Fixes

* check statuses with != instead of !== ([9a87a88](https://bitbucket.org/charleskoehl/ci-helper/commit/9a87a889ce8b10b9fb10b971e219e21be755a070))

### [1.4.9](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.8...v1.4.9) (2019-11-05)

### [1.4.8](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.7...v1.4.8) (2019-11-05)

### [1.4.7](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.6...v1.4.7) (2019-11-05)

### [1.4.6](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.5...v1.4.6) (2019-11-05)

### [1.4.5](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.4...v1.4.5) (2019-11-05)

### [1.4.4](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.3...v1.4.4) (2019-11-05)


### Bug Fixes

* detect failed deployment correctly ([6e1494e](https://bitbucket.org/charleskoehl/ci-helper/commit/6e1494e12fd3a6c40c24ff8da0dad25f5e68b7f7))

### [1.4.3](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.2...v1.4.3) (2019-11-05)


### Bug Fixes

* use BITBUCKET_DEPLOYMENT_ENVIRONMENT ([6ec5a2e](https://bitbucket.org/charleskoehl/ci-helper/commit/6ec5a2efe9d17cfca9582bbcf201c2ed018b2a17))

### [1.4.2](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.1...v1.4.2) (2019-11-05)

### [1.4.1](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.4.0...v1.4.1) (2019-11-05)


### Bug Fixes

* promisify shell.js exec function ([3ea6742](https://bitbucket.org/charleskoehl/ci-helper/commit/3ea6742b699d7c3b3739480882eda902a47681ab))

## [1.4.0](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.3.2...v1.4.0) (2019-11-05)


### Features

* add get-kinvey-status op ([7ec427e](https://bitbucket.org/charleskoehl/ci-helper/commit/7ec427e43249036bb00742f42cefe21224cf8a50))

### [1.3.2](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.3.1...v1.3.2) (2019-11-05)

### [1.3.1](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.3.0...v1.3.1) (2019-11-05)


### Bug Fixes

* require instead of import ([21e0bfc](https://bitbucket.org/charleskoehl/ci-helper/commit/21e0bfcd58698c8ccea447be89382cee12dd8ccd))

## [1.3.0](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.2.0...v1.3.0) (2019-11-05)


### Features

* add wait-kinvey-deploy op ([151ba8a](https://bitbucket.org/charleskoehl/ci-helper/commit/151ba8ab71f634b91372269f32f098cf2bd9b4c4))

## [1.2.0](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.1.8...v1.2.0) (2019-11-05)


### Features

* add wait-kinvey-deploy op ([5de2de9](https://bitbucket.org/charleskoehl/ci-helper/commit/5de2de98b5ce822fb7660c66d36cff7f0e26d522))
* add wait-kinvey-deploy op ([297650b](https://bitbucket.org/charleskoehl/ci-helper/commit/297650bb42ee371cd3720f7419c0b1d53094df78))

### [1.1.8](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.1.7...v1.1.8) (2019-11-01)

### [1.1.7](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.1.6...v1.1.7) (2019-11-01)

### [1.1.6](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.1.5...v1.1.6) (2019-11-01)


### Bug Fixes

* don't copy files when compiling ([40e9c37](https://bitbucket.org/charleskoehl/ci-helper/commit/40e9c3797429474b3f384aeaa137ff8b404315f0))

### [1.1.5](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.1.4...v1.1.5) (2019-11-01)


### Bug Fixes

* bin location ([595f465](https://bitbucket.org/charleskoehl/ci-helper/commit/595f465dbf2ae6899a38f18e6aac1e095dab5ef5))

### [1.1.4](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.1.3...v1.1.4) (2019-11-01)


### Bug Fixes

* bin location ([e1416d5](https://bitbucket.org/charleskoehl/ci-helper/commit/e1416d5a61a02cb10cb0f8357e8dd1d97fe80cd2))

### [1.1.3](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.1.2...v1.1.3) (2019-11-01)


### Bug Fixes

* es6 and use bin dir ([5050f28](https://bitbucket.org/charleskoehl/ci-helper/commit/5050f289041d7a8f276d2e52d3ddca04bb4ec690))

### [1.1.2](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.1.1...v1.1.2) (2019-11-01)


### Bug Fixes

* bin command path ([40632ba](https://bitbucket.org/charleskoehl/ci-helper/commit/40632baa23e639b65651d3d852080f25d2aa6dd9))

### [1.1.1](https://bitbucket.org/charleskoehl/ci-helper/compare/v1.1.0...v1.1.1) (2019-11-01)

## 1.1.0 (2019-11-01)


### Features

* otp function ([75308d9](https://bitbucket.org/charleskoehl/ci-helper/commit/75308d9f63d0ce530642f9b7b77dcd13833be5c9))
