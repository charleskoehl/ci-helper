#!/usr/bin/env node

import * as yup from 'yup'
import _ from 'lodash'
import { eachSeries, every, everySeries, whilst, until } from 'async'
import Promise from 'bluebird'
import { echo, exit } from 'shelljs'
import ConfigStore from 'configstore'
import inquirer from 'inquirer'

const otplib = require('otplib')

const exec = Promise.promisify(require('shelljs').exec)

const argv = require('yargs').argv

const config = new ConfigStore('ci-helper')

/**
 * Returns one-time 2FA password.
 *
 * @param {string} secret
 * @return {string}
 */
const getOtp = async ({
  secret = process.env.KINVEY_USER_SECRET,
  minSecondsRemaining = 3
} = {}) => {
  const secondsRemaining = otplib.authenticator.timeRemaining()
  if (secondsRemaining < minSecondsRemaining) {
    await Promise.delay((secondsRemaining + 1) * 1000)
  }
  return otplib.authenticator.generate(secret)
}

const outputOtp = async params => {
  const token = await getOtp(params)
  console.log(token)
}

/**
 * Waits for Kinvey runtime status of particular service ID to be "ONLINE"
 * Exits with 0 if succeeds, 1 otherwise.
 *
 * Requires these env vars:
 * PACKAGE_VERSION
 * KINVEY_SERVICE_ID
 * BITBUCKET_DEPLOYMENT_ENVIRONMENT
 * KINVEY_USER_SECRET
 *
 * @return {Promise<void>}
 */
const waitForKinveyDeployment = async () => {
  const targets = [
    {
      name: 'Deployment Status',
      key: 'deploymentStatus',
      val: 'COMPLETED',
      errorVal: 'FAILED'
    },
    {
      name: 'Package Version',
      key: 'version',
      val: process.env.PACKAGE_VERSION
    },
    {
      name: 'Runtime Status',
      key: 'status',
      val: 'ONLINE'
    }
  ]
  everySeries(
    targets,
    async ({
      name,
      key,
      val,
      errorVal
    }, callback) => {
      let testVal = await getKinveyStatus({ key })
      whilst(
        cb => {
          echo(`${name}  ->  Target: "${val}"  Current: "${testVal}"`)
          cb(null, testVal !== val)
        },
        cb => {
          if (testVal === errorVal) {
            exit(1)
          } else {
            setTimeout(async () => {
              testVal = await getKinveyStatus({ key })
              cb(null, testVal)
            }, 5000)
          }
        },
        callback
      )
    }
    ,
    err => {
      exit(err ? 1 : 0)
    }
  )
}

const getKinveyStatus = async ({
  key,
  serviceId = process.env.KINVEY_SERVICE_ID,
  serviceEnv = process.env.KINVEY_SERVICE_ENV_NAME,
  ...creds
}) => {
  try {
    const credsFlagsStr = await getKinveyCredsFlagsString(creds)
    try {
      const cmd = `kinvey flex status --service ${serviceId} --env ${serviceEnv} ${credsFlagsStr}| grep "${key}" | awk '{print $2}'`
      const str = await exec(cmd, { async: true, silent: true })
      return str.trim()
    } catch (e) {
      console.log('getKinveyStatus error getting flex status', e)
    }
  } catch (e) {
    console.log('getKinveyStatus error getting token', e)
  }
}

const createKinveyProfile = async ({ name }) => {
  const credsConfig = [
    {
      name: 'instanceId',
      envVarName: 'KINVEY_CLI_INSTANCE_ID',
      message: 'Enter Kinvey Instance ID:',
      validate: val => val.length > 6
        ? true
        : 'Kinvey Instance ID is too short. Please enter again:',
      store: true
    },
    {
      name: 'email',
      envVarName: 'KINVEY_CLI_EMAIL',
      message: 'Enter Kinvey Account Email Address:',
      validate: val => yup.string().email().isValid()
        ? true
        : 'Kinvey Account Email Address in incorrect format. Please enter again:',
      store: true
    },
    {
      name: 'password',
      envVarName: 'KINVEY_CLI_PASSWORD',
      message: 'Enter Kinvey Account Password:',
      validate: val => val.length > 7
        ? true
        : 'Kinvey Account Password is too short. Please enter again:'
    },
    {
      name: 'userSecret',
      envVarName: 'KINVEY_USER_SECRET',
      message: 'Enter Kinvey User Secret:',
      validate: val => val.length > 40 ? true : 'Kinvey User Secret seems too short. Please enter again:'
    }
  ]
  const creds = {}
  const questions = []
  _.forEach(credsConfig, ({
    name,
    envVarName,
    message,
    validate,
    store = false
  }) => {
    if (process.env[envVarName]) {
      creds[name] = process.env[envVarName]
    } else {
      questions.push({
        name,
        type: 'input',
        message,
        validate,
        default: store ? config.get(`kinvey.${name}`) : undefined
      })
    }
  })
  if (!_.isEmpty(questions)) {
    const answers = await inquirer.prompt(questions)
    const answersToStore = _.pick(answers, _.chain(credsConfig).filter('store').map('name').value())
    console.log('Storing answers:', JSON.stringify(answersToStore))
    config.set('kinvey', answersToStore)
    _.assign(creds, answers)
  }
  console.log('All creds:', JSON.stringify(creds))
  try {
    const credsFlagsStr = await getKinveyCredsFlagsString(creds)
    try {
      await exec(`kinvey profile create ${name} ${credsFlagsStr}`, { async: true, silent: false })

      return { creds }
    } catch (e) {
      console.log('createKinveyProfile error calling "kinvey profile create"', e)
    }
  } catch (e) {
    console.log('createKinveyProfile error getting token', e)
  }
}

const setKinveyServiceVars = async ({
  serviceId,
  serviceEnv,
  vars,
  ...creds
}) => {
  console.log(`Setting service vars for service id "${serviceId}"`)
  const credsFlagsStr = await getKinveyCredsFlagsString(creds)
  if(credsFlagsStr) {
    const varsStr = Object.keys(vars).map(key => key + '=' + vars[key]).join(',')
    try {
      return exec(
        `kinvey flex update --service ${serviceId} --env ${serviceEnv} ${credsFlagsStr} --set-vars ${varsStr}`,
        { async: true, silent: false }
      )
    } catch (e) {
      console.log(`Error setting service vars for service ID "${serviceId}"`, e)
    }
  } else {
    console.log(`Could not get creds to set service vars for service ID "${serviceId}"`)
  }
}

const setNeo4jVars = async () => {
  const { env } = await inquirer.prompt([
    {
      name: 'env',
      type: 'list',
      message: 'Kinvey App Environment?',
      choices: [
        'Staging',
        'Production'
      ],
      filter: val => val.toLowerCase()
    }
  ])
  const defaults = config.get(`neo4j.${env}`) || {}
  const questions = [
    {
      name: 'serviceIdsStr',
      type: 'input',
      message: 'Kinvey Service IDs, comma-separated:',
      validate: val => val.length >= 32
        ? true
        : 'Kinvey Service IDs are 32 characters long. Please enter at least one:',
      default: defaults.serviceIdsStr
    },
    {
      name: 'serviceEnv',
      type: 'input',
      message: 'Kinvey Service Environment (for all services):',
      validate: val => val.length >= 3
        ? true
        : 'Kinvey Service Environment is too short. Please enter again:',
      default: defaults.serviceEnv || 'Default'
    },
    {
      name: 'url',
      type: 'input',
      message: 'Neo4j URL',
      validate: val => yup.string().url().isValid(val)
        ? true
        : 'Neo4j URL is in wrong format. Please enter again.',
      default: defaults.url
    },
    {
      name: 'username',
      type: 'input',
      message: 'Neo4j Username:',
      validate: val => val.length > 2 ? true : 'Neo4j Username seems too short. Please enter again:',
      default: defaults.username
    },
    {
      name: 'password',
      type: 'input',
      message: 'Neo4j Password:',
      validate: val => val.length > 2 ? true : 'Neo4j Password seems too short. Please enter again:',
      default: defaults.password
    }
  ]
  const {
    serviceIdsStr,
    serviceEnv,
    url,
    username,
    password
  } = await inquirer.prompt(questions)
  config.set(`neo4j.${env}`, {
    serviceIdsStr,
    serviceEnv,
    url,
    username,
    password
  })
  const serviceIds = serviceIdsStr.split(',')
  const { creds } = await createKinveyProfile({ name: 'dev' })
  if (creds.userSecret) {
    console.log('Setting environment variables.')
    try {
      await eachSeries(serviceIds, async serviceId => {
        try {
          await setKinveyServiceVars({
            serviceId,
            serviceEnv,
            vars: {
              NEO4J_URL: url,
              NEO4J_USERNAME: username,
              NEO4J_PASSWORD: password
            },
            ...creds
          })
        } catch (e) {
          console.log(`Error setting service vars for service ID "${serviceId}"`, e)
        }
      })
      console.log('Waiting for all services to come back online.')
      try {
        await until(
          every(
            serviceIds,
            async serviceId => {
              const status = await getKinveyStatus({
                key: 'status',
                serviceId,
                serviceEnv,
                ...creds
              })
              return status === 'ONLINE'
            }
          )
        )
        console.log('Neo4j variables have been set!')
      } catch (e) {
        console.log('setNeo4jVars error waiting for services to come online', e)
      }
    } catch (e) {
      console.log('setNeo4jVars error setting service vars', e)
    }
  }
}

const getKinveyCredsFlagsString = async ({
  userSecret = process.env.KINVEY_USER_SECRET,
  email = process.env.KINVEY_CLI_EMAIL,
  password = process.env.KINVEY_CLI_PASSWORD,
  instanceId = process.env.KINVEY_CLI_INSTANCE_ID
} = {}) => {
  try {
    const token = await getOtp({ secret: userSecret })
    return `--email ${email} --password ${password} --instanceId ${instanceId} --2fa ${token}`
  } catch (e) {
    console.log('getKinveyCredsFlagsString error getting token:', e)
  }
}

switch (argv.op) {
  case 'otp':
    outputOtp(argv)
    break
  case 'create-kinvey-profile':
    createKinveyProfile(argv)
    break
  case 'wait-kinvey-deploy':
    waitForKinveyDeployment()
    break
  case 'set-neo4j-vars':
    setNeo4jVars()
}
